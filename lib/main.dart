import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pub_shower/src/screens/banner_screen.dart';
import 'package:pub_shower/src/screens/interstitial_screen.dart';
import 'package:pub_shower/src/screens/settings.dart';
import 'package:pub_shower/src/stores/publicity_store.dart';
import 'package:pub_shower/src/stores/screen_store.dart';
import 'src/screens/home_page.dart';
import 'src/const.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          builder: (_) => PublicityStore(),
        ),
        ChangeNotifierProvider(
          builder: (_) => ScreenStore(),
        ),
      ],
      child: MaterialApp(
        initialRoute: homeScreen,
        routes: {
          homeScreen: (context) => MyHomePage(),
          bannerScreen: (context) => BannerScreen(),
          interstitialScreen: (context) => InterstitialScreen(),
          settingsScreen: (context) => Settings(),
        },
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
      ),
    );
  }
}
