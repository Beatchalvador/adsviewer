import 'package:flutter/material.dart';

class ScreenStore with ChangeNotifier {
  int _currentScreen = 0;

  int get currentScreen => _currentScreen;
  set currentScreen(int value) {
    _currentScreen = value;
    notifyListeners();
  }
}
