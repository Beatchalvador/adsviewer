import 'package:flutter/material.dart';
import 'package:pub_shower/src/models/publicity.dart';

class PublicityStore with ChangeNotifier {
  Publicity publicity = Publicity();

  get keywords => publicity.keywords;

  set keywords(Set<String> values) {
    notifyListeners();
  }

  void searchAds() {
    publicity.serachAds();
  }

  void reRender() {
    notifyListeners();
  }

  get banner => publicity.banner;
  void disposeBanner() {
    publicity.disposeMyBanner();
    print('dezte men han');
    notifyListeners();
  }

  get interstitial => publicity.interstitial;
  void disposeInterstitial() {
    publicity.disposeMyInterstitial();
    notifyListeners();
  }
}
