import 'package:firebase_admob/firebase_admob.dart';
import '../const.dart';

class Publicity {
  Set<String> keywords;
  MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    keywords: [],
    testDevices: <String>[],
  );
  BannerAd banner;
  BannerAd largeBanner;
  InterstitialAd interstitial;

  Publicity() {
    FirebaseAdMob.instance.initialize(appId: appId);
    keywords = new Set<String>();
    banner = initMyBanner(null);
    interstitial = initMyInterstitial(null);
  }

  BannerAd initMyBanner(MobileAdTargetingInfo tInfo) {
    targetingInfo = tInfo ?? targetingInfo;
    return BannerAd(
      adUnitId: banner_adId,
      size: AdSize.banner,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        if (event == MobileAdEvent.failedToLoad) {
          banner..load();
        } else if (event == MobileAdEvent.closed) {
          banner.dispose();
          banner = initMyBanner(targetingInfo)..load();
        }
      },
    );
  }

  void disposeMyBanner() {
    print('hani disposite banner');
    banner?.dispose();
    banner = initMyBanner(targetingInfo);
  }

  InterstitialAd initMyInterstitial(MobileAdTargetingInfo tInfo) {
    targetingInfo = tInfo ?? targetingInfo;
    return InterstitialAd(
      adUnitId: interstitiel_adId,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        if (event == MobileAdEvent.failedToLoad) {
          interstitial..load();
        } else if (event == MobileAdEvent.closed) {
          interstitial.dispose();
          interstitial = initMyInterstitial(tInfo)..load();
        }
      },
    );
  }

  void disposeMyInterstitial() {
    print('hani disposite interstitial');
    interstitial?.dispose();
    interstitial = initMyInterstitial(targetingInfo);
  }

  void serachAds() {
    print('Hani wsselte l search ads ');
    print(keywords);
    targetingInfo = MobileAdTargetingInfo(
      keywords: keywords.toList(),
      testDevices: <String>[],
    );
    print('Ha targetting ila banlek');
    print(targetingInfo.keywords);
    banner?.dispose();
    banner = initMyBanner(targetingInfo);
    interstitial?.dispose();
    interstitial = initMyInterstitial(targetingInfo);
  }
}
