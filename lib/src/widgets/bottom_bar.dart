import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:pub_shower/src/stores/publicity_store.dart';
import '../stores/screen_store.dart';
import 'package:provider/provider.dart';
import '../const.dart';

class BottomBar extends StatelessWidget {
  Widget build(BuildContext context) {
    var screenStore = Provider.of<ScreenStore>(context);
    var publicityStore = Provider.of<PublicityStore>(context);
    return BottomNavyBar(
      selectedIndex: screenStore.currentScreen,
      showElevation: true, // use this to remove appBar's elevation
      onItemSelected: (index) {
        publicityStore.disposeBanner();
        publicityStore.disposeInterstitial();
        Navigator.pushNamed(context, namedRoutes[index]);
        screenStore.currentScreen = index;
      },
      items: [
        BottomNavyBarItem(
          icon: Icon(Icons.apps),
          title: Text('Home'),
          activeColor: Colors.red,
        ),
        BottomNavyBarItem(
            icon: Icon(Icons.line_weight),
            title: Text('Banner'),
            activeColor: Colors.purpleAccent),
        BottomNavyBarItem(
            icon: Icon(Icons.play_circle_filled),
            title: Text('Interstitial'),
            activeColor: Colors.pink),
        BottomNavyBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
            activeColor: Colors.blue),
      ],
    );
  }
}

//
