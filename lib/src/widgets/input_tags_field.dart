import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:pub_shower/src/stores/publicity_store.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class InputTagsField extends StatefulWidget {
  InputTagsField({Key key}) : super(key: key);

  _InputTagsFieldState createState() => _InputTagsFieldState();
}

class _InputTagsFieldState extends State<InputTagsField> {
  TextEditingController _controller = TextEditingController();
  String keyword = '';

  void errorAlert(BuildContext context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: 'Error',
      desc: 'Only five keywords are allowed',
      buttons: [
        DialogButton(
          child: Text(
            "Close",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.blue,
        ),
      ],
    ).show();
  }

  void addKeyword(publicityStore, context) {
    int length = publicityStore.keywords.toList().length;
    if (length == 5) {
      errorAlert(context);
    } else if (length < 6) {
      publicityStore.keywords.add(keyword);
      _controller.clear();
      publicityStore.reRender();
    }
  }

  @override
  Widget build(BuildContext context) {
    var publicityStore = Provider.of<PublicityStore>(context);
    return Row(
      children: <Widget>[
        Expanded(
          child: SizedBox(),
          flex: 1,
        ),
        Expanded(
          flex: 9,
          child: TextField(
            controller: _controller,
            onSubmitted: (value) {
              addKeyword(publicityStore, context);
            },
            onChanged: (value) {
              keyword = value;
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Enter Keywords",
                labelText: "Keyword"),
          ),
        ),
        Expanded(
          flex: 6,
          child: FlatButton(
            onPressed: () {
              addKeyword(publicityStore, context);
            },
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.add,
                  color: Colors.blue,
                ),
                Text(
                  'Add keyword',
                  style: TextStyle(color: Colors.blue),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
