import 'package:flutter/material.dart';
import 'package:pub_shower/src/widgets/input_tags_field.dart';
import 'keyword_container.dart';

class PubContainer extends StatelessWidget {
  final Function searchForAds;
  final String buttonText;
  final Set<String> keywords;
  PubContainer(
      {@required this.searchForAds,
      @required this.buttonText,
      @required this.keywords});

  Widget wrapKeywords(Set<String> keywords) {
    List<Widget> wrapChildren = new List<Widget>();
    // && keywords.length < 6
    if (keywords != null && keywords.length > 0) {
      wrapChildren = keywords
          .map((keyword) => KeywordContainer(
                keyword: keyword,
              ))
          .toList();
    }
    return Center(
      child: Wrap(
        spacing: 15.0,
        children: wrapChildren,
        alignment: WrapAlignment.center,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 3,
          child: InputTagsField(),
        ),
        Expanded(
          flex: 4,
          child: wrapKeywords(keywords),
        ),
        Expanded(
          flex: 2,
          child: RawMaterialButton(
            fillColor: Colors.blue,
            elevation: 6.0,
            textStyle: TextStyle(color: Colors.white, fontSize: 33.0),
            onPressed: searchForAds,
            child: Text(this.buttonText),
          ),
        ),
        Expanded(
          flex: 13,
          child: SizedBox(),
        )
      ],
    );
  }
}
