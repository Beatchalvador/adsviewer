import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pub_shower/src/stores/publicity_store.dart';

class KeywordContainer extends StatelessWidget {
  final String keyword;
  const KeywordContainer({this.keyword});

  @override
  Widget build(BuildContext context) {
    var publicityStore = Provider.of<PublicityStore>(context);
    return Chip(
      deleteIcon: Icon(Icons.close),
      deleteIconColor: Colors.white,
      backgroundColor: Colors.blue,
      label: Text(
        keyword,
        style: TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ),
      ),
      onDeleted: () {
        publicityStore.keywords.removeWhere((kw) => kw == keyword);
        publicityStore.reRender();
      },
    );
  }
}
