const String appId = 'ca-app-pub-9897997442609743~3310770455';
const String banner_adId = 'ca-app-pub-9897997442609743/5230356785';
const String interstitiel_adId = 'ca-app-pub-9897997442609743/2239848477';

const String homeScreen = '/';
const String bannerScreen = '/banner';
const String interstitialScreen = '/interstitial';
const String settingsScreen = '/settings';
const List<String> namedRoutes = [
  homeScreen,
  bannerScreen,
  interstitialScreen,
  settingsScreen
];
