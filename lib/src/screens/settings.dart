import 'package:flutter/material.dart';
import 'package:pub_shower/src/widgets/bottom_bar.dart';

class Settings extends StatelessWidget {
  const Settings({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text('Settings'),
          centerTitle: true,
        ),
        bottomNavigationBar: BottomBar(),
      ),
    );
  }
}
