import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:pub_shower/src/stores/publicity_store.dart';
import 'package:pub_shower/src/widgets/bottom_bar.dart';
import 'package:pub_shower/src/widgets/pub_container.dart';

class InterstitialScreen extends StatelessWidget {
  const InterstitialScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var publicityStore = Provider.of<PublicityStore>(context);
    var height = MediaQuery.of(context).size.height;
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          automaticallyImplyLeading: false,
          title: Text(
            'Interstitial Screen',
            style: TextStyle(color: Colors.blue),
          ),
          centerTitle: true,
        ),
        body: PubContainer(
          buttonText: 'Search for interstitial',
          searchForAds: () {
            publicityStore.searchAds();
            publicityStore.disposeInterstitial();
            publicityStore.interstitial
              ..load()
              ..show();
            SystemChannels.textInput.invokeMethod('TextInput.hide');
            print('hanta kat9lleb 3la intestitial a dak jene');
            print(publicityStore.keywords);
          },
          keywords: publicityStore.keywords,
        ),
        bottomNavigationBar: BottomBar(),
      ),
    );
  }
}
