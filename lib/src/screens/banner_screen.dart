import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:pub_shower/src/stores/publicity_store.dart';
import 'package:pub_shower/src/widgets/bottom_bar.dart';
import 'package:pub_shower/src/widgets/pub_container.dart';

class BannerScreen extends StatelessWidget {
  const BannerScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var publicityStore = Provider.of<PublicityStore>(context);
    var height = MediaQuery.of(context).size.height;
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          title: Text(
            'Banner Screen',
            style: TextStyle(color: Colors.blue),
          ),
          centerTitle: true,
        ),
        body: PubContainer(
          buttonText: 'Search for banner',
          searchForAds: () {
            publicityStore.searchAds();
            publicityStore.disposeBanner();
            publicityStore.banner
              ..load()
              ..show(
                anchorOffset: height / 1.5,
                horizontalCenterOffset: 000.0,
                anchorType: AnchorType.top,
              );
            SystemChannels.textInput.invokeMethod('TextInput.hide');
          },
          keywords: publicityStore.keywords,
        ),
        bottomNavigationBar: BottomBar(),
      ),
    );
  }
}
